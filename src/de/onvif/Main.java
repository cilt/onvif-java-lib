package de.onvif;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.util.List;

import javax.xml.soap.SOAPException;

import org.onvif.ver10.device.wsdl.Service;
import org.onvif.ver10.schema.Profile;

import de.onvif.soap.OnvifDevice;

public class Main {

	private static final String INFO = "Commands:\n  \n  url: Get snapshort URL.\n  info: Get information about each valid command.\n  exit: Exit this application.";

	public static void main(String args[]) {
		InputStreamReader inputStream = new InputStreamReader(System.in);
		BufferedReader keyboardInput = new BufferedReader(inputStream);
		String input
			,cameraAddress = "ac-cc-8e-30-46-77.lan.uct.ac.za"
			,user = "admin"
			,password = "password";
		
		/*
		try {
			System.out.println("Please enter camera IP (with port if not 80):");
			cameraAddress = keyboardInput.readLine();
			System.out.println("Please enter camera username:");
			user = keyboardInput.readLine();
			System.out.println("Please enter camera password:");
			password = keyboardInput.readLine();
		}
		catch (IOException e1) {
			e1.printStackTrace();
			return;
		}
		*/

		System.out.println("Connect to camera, please wait ...");
		OnvifDevice cam;
		try {
			cam = new OnvifDevice(cameraAddress, user, password);
		}
		catch (ConnectException | SOAPException e1) {
			System.err.println("No connection to camera, please try again.");
			return;
		}
		System.out.println("Connection to camera successful!");

		/*
		List<Service> services = cam.getDevices().getServices(true);
		
		for (Service item : services) {
			
			System.out.println( item.getCapabilities().toString());
		}
		*/
		List<Profile> profiles = cam.getDevices().getProfiles();
		String profileToken = profiles.get(0).getToken();
		
		cam.getSoap().setLogging(true);
		
		
		
		while (true) {
			try {
				System.out.println();
				System.out.println("Enter a command (type \"info\" to get commands):");
				input = keyboardInput.readLine();

				switch (input) {
				case "home":
					cam.getPtz().moveHome(profileToken);
					break;
				case "u":
					cam.getPtz().absoluteMove(profileToken, 0.0f, 1.0f, 0.0f);
					break;
					
				case "l":
					cam.getPtz().absoluteMove(profileToken, -1.0f, 0.0f, 0.0f);
					break;
					
				case "ul":
					cam.getPtz().absoluteMove(profileToken, -0.75f, 0.75f, 0.0f);
					break;					

				case "uls":
					cam.getPtz().absoluteMove(profileToken, -0.75f, 0.75f, 0.0f, 0.05f, 0.05f, 0.0f);
					break;					
				case "url": {
					List<Profile> profiless = cam.getDevices().getProfiles();
					for (Profile p : profiless) {
						System.out.println("URL von Profil \'" + p.getName() + "\': " + cam.getMedia().getSnapshotUri(p.getToken()));
					}
					break;
				}
				case "profiles":
					List<Profile> profiless = cam.getDevices().getProfiles();
					System.out.println("Number of profiles: " + profiless.size());
					break;
				case "info":
					System.out.println(INFO);
					break;
				case "quit":
				case "exit":
				case "end":
					System.out.println("Done.");
					return;
				default:
					System.out.println("Unknown command!");
					System.out.println();
					System.out.println(INFO);
					break;
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (SOAPException e) {
				e.printStackTrace();
			}
		} // While
	}
}